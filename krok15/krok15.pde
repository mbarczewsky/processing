class Czasomierz{
  private int interval=0;
  private int ostatniCzas=0;
  
  Czasomierz(int interval){
    this.interval=interval;
  }
  
  public boolean czasMinal()
  {
    if ((millis()-ostatniCzas)>interval)
    {
      ostatniCzas=millis();
      return true;
    }
    else return false;
  }
}

class Obraz
{
  private Czasomierz nastepnaKlatka = new Czasomierz(65);
  private Czasomierz przerwa = new Czasomierz(0);
  private ArrayList<PImage> tablicaObrazu;
  private int frame=0;
  private boolean animacjaTrwa=true;
  private boolean kolceDoGory=true;
  
  Obraz(String sciezka,int szer, int wys,int wiersze, int kolumny)
  {
    int tempCount=0;
    tablicaObrazu= new ArrayList<PImage>();
    PImage temp = loadImage(sciezka);
    
    for(int i=0;i<kolumny;i++){
      for(int j=0;j<wiersze;j++){
        tablicaObrazu.add(tempCount,temp.get(j*szer,wys*i,szer,wys));
        tempCount++;
      }
    }
    
  }
  
  public void rysuj(int posX, int posY){
    if(przerwa.czasMinal())animacjaTrwa=true;
    
    if(animacjaTrwa){
      if(nastepnaKlatka.czasMinal()){
        if(kolceDoGory)frame++;
        else frame--;
      }
      if(!(frame<tablicaObrazu.size()-1)){
        kolceDoGory=false;
        animacjaTrwa=false;
      }
      if(frame<=0){
        kolceDoGory=true;
        animacjaTrwa=false;
      }
    }
    image(tablicaObrazu.get(frame),posX,posY);
  }
  
  public void usunKlatki(int ile){
    for(int i=0;i<ile;i++){
      tablicaObrazu.remove(tablicaObrazu.size()-1);
    }
  }
  
  public void setPrzerwa(int czas){
    przerwa = new Czasomierz(czas);
  }
}

class Kolec{
  private Obraz kolec;
  
  Kolec(){
    kolec = new Obraz("kolec.png",115,200,6,7);
    kolec.usunKlatki(2);
    kolec.setPrzerwa(5000);
  }
  
  public void rysuj(int a, int b){
    kolec.rysuj(a,b);
  }
}

class Ptak{
  private Obraz ptaszor;
  private int szer=110;
  private int wys=101;
  
  Ptak(){
    ptaszor = new Obraz("bird.png",110,101,5,3);
    ptaszor.usunKlatki(1);
  }
  
  public void rysuj(int posX,int posY){
    ptaszor.rysuj(posX-szer/2,posY-wys/2);
  }
}

Ptak ptaszek;
Kolec jedenKolec;

void setup(){
  size(400,400);
  jedenKolec = new Kolec();
  ptaszek = new Ptak();
}

void draw(){
  background(#FFFFFF);

  jedenKolec.rysuj(300,100);
  ptaszek.rysuj(mouseX,mouseY);
  
  
}
