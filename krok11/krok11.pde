PImage kobieta4x3;
PImage[] kobieta = new PImage[12];
int szer=110;
int wys=151;
int x=0;
int y=20;
int kroki=6;
int ostatniCzas=0;
int ktoryObraz=0;
String robiKrok="nie";
void setup(){
  frameRate(30);
  size(500,200);
  kobieta4x3=loadImage("womanwalk4x3-110x151.png");
  for(int i=0;i<kobieta.length;i++){
    if(i<4){
      kobieta[i]=kobieta4x3.get(i*szer,0,szer,wys);
    }
    else if(i<8){
      kobieta[i]=kobieta4x3.get((i-4)*szer,wys,szer,wys);
    }
    else{
      kobieta[i]=kobieta4x3.get((i-8)*szer,wys*2,szer,wys);
    }
  }
}
void zrobKrok(String krok){
  if(robiKrok!="nie")return;
  if(krok=="lewo"){
    kroki=6;
    robiKrok="left";
  }
  if(krok=="prawo"){
    kroki=6;
    robiKrok="right";
  }
}

void charMove(String arg){
  //ten warunek sprawia że obrazki nie zmieniaja sie zbyt szybko mimo wysokiego frameRate
  if(!nastepnyObraz())return;
  if(arg=="nie")return;
  if(arg=="left"){
    x-=11;
    ktoryObraz--;
  }
  if(arg=="right"){
    x+=11;
    ktoryObraz++;
  }
  kroki--;
  if(kroki<1)robiKrok="nie";
  if(ktoryObraz>11)ktoryObraz=0;
  if(ktoryObraz<0)ktoryObraz=11;
}

void draw() {
  background(#FFFFFF);
  charMove(robiKrok);
  image(kobieta[ktoryObraz],x,y);
}

void keyPressed(){
  if(key == CODED){
    if (keyCode == LEFT)zrobKrok("lewo");
    if (keyCode == RIGHT)zrobKrok("prawo");
  }
}

//funkcja pożyczona z zadania z rybą
boolean nastepnyObraz()
{
  if ((millis()-ostatniCzas)>50)
  {
      ostatniCzas=millis();
      return true;
  }
  else return false;
}
