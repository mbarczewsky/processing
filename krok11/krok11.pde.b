PImage kobieta4x3;
PImage[] kobieta = new PImage[12];
int szer=110;
int wys=151;
int x=0;
int y=20;
int ostatniCzas=0;
int ktoryObraz=0;
void setup(){
  frameRate(60);
  size(500,200);
  kobieta4x3=loadImage("womanwalk4x3-110x151.png");
  for(int i=0;i<kobieta.length;i++){
    if(i<4){
      kobieta[i]=kobieta4x3.get(i*szer,0,szer,wys);
    }
    else if(i<8){
      kobieta[i]=kobieta4x3.get((i-4)*szer,wys,szer,wys);
    }
    else{
      kobieta[i]=kobieta4x3.get((i-8)*szer,wys*2,szer,wys);
    }
  }
}

void charMove(String arg){
  //ten warunek sprawia że obrazki nie zmieniaja sie zbyt szybko mimo wysokiego frameRate
  if(!nastepnyObraz())return;
  
  if(arg=="left"){
    x-=11;
    ktoryObraz--;
  }
  if(arg=="right"){
    x+=11;
    ktoryObraz++;
  }
  if(ktoryObraz>11)ktoryObraz=0;
  if(ktoryObraz<0)ktoryObraz=11;
}

void draw() {
  background(#FFFFFF);
  image(kobieta[ktoryObraz],x,y);
}

void keyPressed(){
  if(key == CODED){
    if (keyCode == LEFT)charMove("left");
    if (keyCode == RIGHT)charMove("right");
  }
}

//funkcja pożyczona z zadania z rybą
boolean nastepnyObraz()
{
  if ((millis()-ostatniCzas)>50)
  {
      ostatniCzas=millis();
      return true;
  }
  else return false;
}
