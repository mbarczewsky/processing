/* Tu ładujemy obrazki przed zakonczeniu - pliki powinną byc w tym samym folderu gdzie ten plik jest
/* @pjs preload="ball.png"; */


ArrayList<Pilka> tablicaPilek;
Pilkarz facet;

class Pilka{
  PImage pilkaObraz;
  int rozmiarPilki=50;

  boolean pilkaIdzieNaDol=true;
  int prawoLewoPilka=3;
  int pilkaX,pilkaY;
  int szybkosc=8;
  
  Pilka(){
    pilkaObraz = loadImage("ball.png");
    
    pilkaX=Math.round(random(50,650));   //pilka zaczny od losowego miejscu, na X (minimum 50, max 650)
    pilkaY=50;      //ale pilka zawsze zaczyna od gory,czyli na Y
    szybkosc=(int)random(5,12);
    prawoLewoPilka=(int)random(-3,3);
  }
  
  void rysuj(){
    przesun();
    image(pilkaObraz,pilkaX,pilkaY,rozmiarPilki,rozmiarPilki); 
  }
  
  void przesun(){
    if (pilkaIdzieNaDol)
    {
      pilkaY+=szybkosc;
    }
    else
    {
      pilkaY-=szybkosc;
    }
    if (pilkaY<5) {pilkaIdzieNaDol=true;}  //pilka dotknęła jeszcze raz góry płótna, idź na dole teraz
    if (pilkaX+rozmiarPilki>690) {prawoLewoPilka=-prawoLewoPilka;}   //pilka dotknęła ramę na prawo
    if (pilkaX<5) {prawoLewoPilka=abs(prawoLewoPilka);}               //pilka dotknęła ramę na lewo
    
    //Zmodifikuj miejsce X Pilki
    pilkaX+=prawoLewoPilka;


    //czy pilka padla na ziemi!!!!
    if (pilkaY>495-rozmiarPilki)
    {
      pilkaIdzieNaDol=false;  //pilka dotknęła jeszcze raz dół płótna, idź na górze teraz
    }
  }
}

class Pilkarz{
  private PImage player = new PImage();
  private float szer,wys;

  Pilkarz(){
    float rozmiarPilkarza = 0.3; //ustala rozmiar pilkarza
    player = loadImage("player.png");
    szer=367*rozmiarPilkarza;
    wys=859*rozmiarPilkarza;
  }
  
  public void rysuj(){
    image(player,mouseX-(szer/2),500-wys,szer,wys);
  }
}



void setup()  //To robimy tyko na początek (tylko raz)
{      
    //rozmiar płótna
    size(700, 500);
    
    //ile razy co sekund mamy odświezyć płótno
    frameRate(30);
    facet = new Pilkarz();
    tablicaPilek = new ArrayList<Pilka>();
    
    for(int i=0;i<10;i++) tablicaPilek.add(new Pilka());
} 


void draw()                //Tą funkcję bedziemy wywołać tyle razy co sekund - aby odświezyć płótno 
{  
    //tlo
    background(#BFDAE6);   //Kolor tła - niebieski
    stroke(#000000);      //Kolor obramowanie prostokąta
    fill(#378B2E);        //Kolor tła prostokąta
    rect(0,480,700,500);  //prostokąt/trawa
    fill(#CC3333);        //Kolor tła tekstu
    //rysowanie pilek
    for(int i=0;i<tablicaPilek.size();i++){
      tablicaPilek.get(i).rysuj();
    }
    
    facet.rysuj();
}
