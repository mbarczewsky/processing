Obraz bomba;

//zrobilem klase ladujaca obraz i konstruktor ladujacy pojedyncze klatki animacji
class Obraz{
  public PImage[] tablicaObrazu;
  private int szer,wys;
  private int frame=0;
  private int myszkaX,myszkaY;
  private boolean animacjaTrwa=false;
  Obraz(String sciezka,int szer, int wys,int x, int y){
    this.szer=szer;
    this.wys=wys;
    
    tablicaObrazu=new PImage[x*y];
    PImage temp = loadImage(sciezka);
    for(int i=0;i<y;i++){
      for(int j=0;j<x;j++){
        tablicaObrazu[i*y+j]=temp.get(j*szer,wys*i,szer,wys);
      }
    }
  }
  
  public void ustawPozycje(int tempX, int tempY){
    if(animacjaTrwa)return;
    animacjaTrwa=true;
    myszkaX=tempX-szer/2;
    myszkaY=tempY-wys/2;
  }
  
  public void animacja(){
    if(!(animacjaTrwa))return;
    
    if(!(frame<tablicaObrazu.length)){
      frame=0;
      animacjaTrwa=false;
      return; //bez return w tym miejscu narysowalo by tablicaObrazu[0]
    }
    image(tablicaObrazu[frame],myszkaX,myszkaY);
    frame++;
  }
}


void setup(){
  background(#FFFFFF);
  frameRate(60);
  size(800,600);
  bomba = new Obraz("bomb9x9-100x100.png",100,100,9,9);
}


void draw(){
  background(#FFFFFF);
  bomba.animacja();
}


void mousePressed(){
  bomba.ustawPozycje(mouseX,mouseY);
}
