PImage[] anim = new PImage[8];
PImage runner;
int szer=432;
float posX=0;
int wys=280;
int frame=0;
float predkosc=14;
void setup(){
  frameRate(10);
  size(500,141);
  runner=loadImage("runboy4x2-108x140.png");
  for(int i=0;i<anim.length;i++){
    if(i<4){
      anim[i]=runner.get(i*szer/4,0,(szer/4),wys/2);
    }
    else{
      anim[i]=runner.get((i-4)*szer/4,wys/2,(szer/4),wys/2);
    }
  }
}
void draw(){
  background(#FFFFFF);
  image(anim[frame%anim.length],posX,0);
  
  //sprawdza czy postać nie jest za oknem 
  if(posX>500){
    posX=-30;
    predkosc=14;
  }
  
  //predkosc nie spada poniżej pewnej wartości, predkosc zmniejsza sie coraz wolniej
  if(predkosc>4)predkosc-=0.025*predkosc;  
  posX+=predkosc;
  frame++;
  
  
}
