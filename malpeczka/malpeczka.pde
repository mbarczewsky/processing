class Malpeczka{
  private float litry=0;
  private float maxPojemnosc=0.1;
  
  public float ileJest(){
    return litry;
  }
  
  public void nalac(float ile){
    if(ile+litry>maxPojemnosc)return;
    litry+=ile;
  }
  
  public float przelac(float ile){
    if(!(litry>0))return 0;
    if(ile<litry){
    litry-=ile;
    }
    else{
      ile=litry;
      litry=0;
    }
    return ile;
  }
}
class Kieliszek{
  private float pojemnosc=0.3;
  private float litry=0;
  
  public void napelnij(float ile){
    if(litry+ile>pojemnosc)return;
    litry+=ile;
  }
  
  public void wyzeruj(){
    litry=0;
  }
}

void setup(){
  Malpeczka cytrynowka = new Malpeczka();
  Kieliszek moj = new Kieliszek();
  cytrynowka.nalac(0.1);
  
  do{
  moj.napelnij(cytrynowka.przelac(0.03));
  moj.wyzeruj();
  
  println("wypilem, zostało",cytrynowka.ileJest());
  }
  while(cytrynowka.ileJest()>0);
  println(cytrynowka.ileJest());
}
