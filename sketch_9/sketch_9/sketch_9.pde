PImage[] anim = new PImage[6];
//test git
PImage runner;
int szer=327;
int wys=282;
int frame=0;
void setup(){
  frameRate(10);
  size(500,141);
  runner=loadImage("runner.png");
  for(int i=0;i<anim.length;i++){
    if(i<3){
      anim[i]=runner.get(i*szer/3,0,(szer/3),wys/2);
    }
    else{
      anim[i]=runner.get((i-3)*szer/3,wys/2,(szer/3),wys/2);
    }
  }
}
void draw(){
  background(#FFFFFF);
  image(anim[frame%anim.length],400-frame*14,0);
  if(frame>36)noLoop();
  frame++;
}
