//Nie dotykaj tej klasy
//Zapamięta miejsce x i y myszki w jednym obiekcie
class Myszka
{
   private float myszkaX;
   private float myszkaY;
  
   //Aktualizuje miejsce myszki
   public void aktualizujMiejsceXY(float x,float y)
   {
      myszkaX=x;
      myszkaY=y;
   }
   
   //Weź X tej myszki
   public float getMyszkaX()
   {
      return myszkaX;
   }
   
   //Weź Y tej myszki
   public float getMyszkaY()
   {
      return myszkaY;
   }
}