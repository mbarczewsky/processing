//Tworzy Przycisk
/**
* UWAGA:
* Nie dotknij ten kod - tu nic nie rób.
*/
class Przycisk
{
  //Nie dotykaj tych zmiennych
  private Miejsce mojeMiejsce;
  private Szerokosc mojSzerok;
  private String mojNapis;
  private boolean mamMyszke=false;
  
  //Tworzy przycisk
  //Nie dotykaj tej metody
  Przycisk(Miejsce mj,Szerokosc szer,String napis)
  {
    mojeMiejsce=mj;
    mojSzerok=szer;
    mojNapis=napis;
  }
  
  //Narysuje przycisk
  //Nie dotykaj tej metody
  public void narysuj()
  {
     if (mamMyszke)
     {
       fill(#39F002);
     }
     else
     {
       fill(#239B00);
     }
     rectMode(CENTER);
     rect(mojeMiejsce.getMX(),mojeMiejsce.getMY(),
           mojSzerok.getSzerokosc(),40,3);

     fill(255);
     stroke(255);
     textSize(23);
     textAlign(CENTER);
     text(mojNapis.toUpperCase(),
           mojeMiejsce.getMX(),mojeMiejsce.getMY()+9);
  }
  
  //Czy myszka jest nad przyciskiem? Daje True/false
  //Nie dotykaj tej metody
  private boolean myszkaNadeMna(Myszka mysz)
  {
    if ((mysz.getMyszkaX()>mojeMiejsce.getMX()-(mojSzerok.getSzerokosc()/2)) &&
        (mysz.getMyszkaX()<mojeMiejsce.getMX()+(mojSzerok.getSzerokosc()/2)) &&
        (mysz.getMyszkaY()>mojeMiejsce.getMY()-20) &&
        (mysz.getMyszkaY()<mojeMiejsce.getMY()+20))
    {
      return true;
    }
    return false;
  }
  
  //Czy myszka jest nad tym przyciskiem? Daje True/false
  //Nie dotykaj tej metody
  public boolean sprawdzCzyMyszkaJestNadeMna(Myszka mysz)
  {
    if (myszkaNadeMna(mysz))
    {
      mamMyszke=true;
      return true;
    }
    mamMyszke=false;
    return false;
  }
  
  //Czy myszka kliknela nad ten przycisk? Daje True/false
  //Nie dotykaj tej metody
  public boolean sprawdzCzyMyszkaKliknelaTu(Myszka mysz)
  {
    if (myszkaNadeMna(mysz))
    {
      return true;
    }
    return false;
  }
  
}