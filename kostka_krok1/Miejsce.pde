//Nie dotykaj tej klasy
//Zapamięta miejsce x i y w jednym obiekcie
class Miejsce
{
  private int miejsceX;
  private int miejsceY;
  
  //Tworzy miejsce
  Miejsce(int mx,int my)
  {
    miejsceX=mx;
    miejsceY=my;
  }
  
  //Weź miejsce X
  public int getMX()
  {
     return miejsceX;
  }
  
  //Weź miejsce Y
  public int getMY()
  {
     return miejsceY;
  }
}