//Tworzy Kropka
class Kropka
{
   //Nie dotykaj tych zmienncyh
   private Farba mojaFarba;
   private Miejsce mojeMiejsce;
   private int srednica=0;
   
   //Ustawia Farbę
   //Nie dotykaj tej metody
   public void setFarba(Farba mfarb)
   {
     mojaFarba=mfarb;
   }
  
   //Ustawia Miejsce
   //Nie dotykaj tej metody
   public void setMiejsce(Miejsce mj)
   {
     mojeMiejsce=mj;
   }
   
   //Ustawia Srednicę
   //Nie dotykaj tej metody
   public void setSrednica(int sred)
   {
     srednica=sred;
   }
  
   //Maluje Kropkę
   //Nie dotykaj tej metody
   public void maluj()
   {
       if (mojeMiejsce==null)
       {
          println("Kropka: Nie podałes mi miejsca!");
          return;
       }
       if (srednica==0)
       {
          println("Kropka: Nie podałes mi srednicy!");
       }
       
       noFill();
       ellipseMode(CENTER);
       if (mojaFarba!=null)
       {
           mojaFarba.aplikuj();
       }
       else
       {
          stroke(0);
          ellipse(mojeMiejsce.getMX(),mojeMiejsce.getMY(),srednica+1,srednica+1);
          println("Kropka: Nie podałes mi farby!");
       }
       noStroke();
       ellipse(mojeMiejsce.getMX(),mojeMiejsce.getMY(),srednica,srednica);
   }
}