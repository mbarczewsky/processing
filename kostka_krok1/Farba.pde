/**
* UWAGA:
* Nie dotknij ten kod - tu nic nie rób.
*/
class Farba
{
   //Domyślna farba - szara
   protected int czer=192;
   protected int ziel=192;
   protected int nieb=192;
   
   //aplikuje Farbę
   public void aplikuj()
   {
      color farbaCol=color(czer,ziel,nieb); 
      fill(farbaCol);
   }
}