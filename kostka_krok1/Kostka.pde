class Kostka
{
  //Nie dotykaj tych zmiennych
  private Rozmiar mojRozmiar;
  private Miejsce mojeMiejsce;
  private Farba mojaFarba;
  private ArrayList<Kropka> tablicaZKropkami;
  private ArrayList<Miejsce[]> wszystkieMiejsca;
  
  
  //Tworzy nową Kostkę
  //Nie dotykaj tej metody
  Kostka(Miejsce mj,Rozmiar roz,Farba frb)
  {
      mojRozmiar=roz;
      mojaFarba=frb;
      mojeMiejsce=mj;
      
      tablicaZKropkami=new ArrayList<Kropka>();
      wszystkieMiejsca=new ArrayList<Miejsce[]>();
      aktualizujMiejscaKropek();
  }

  
  //Skasuje jedna kropka z kostki
  //Nie dotykaj tej metody
  public void skasujJednaKropka()
  {
    if (tablicaZKropkami.size()>0)
    {
      tablicaZKropkami.remove(tablicaZKropkami.size() - 1);
      aktualizujMiejscaKropek();
    }
    else
    {
      println("Kostka: Nie ma Kropek w tej kostce");
      return; 
    }
  }
  
  //Aktualizuje miejsca kropek w kostki
  //Nie dotykaj tej metody
  private void aktualizujMiejscaKropek()
  {
      //tworzenie tablicy z tablicami miejsc do kropek
      int jednaTrzecia=(mojRozmiar.getRozmiar()/3);
      Miejsce srodka=new Miejsce(mojeMiejsce.getMX(),mojeMiejsce.getMY());
      Miejsce lwgr=new Miejsce(mojeMiejsce.getMX()-jednaTrzecia,mojeMiejsce.getMY()-jednaTrzecia);
      Miejsce lwsr=new Miejsce(mojeMiejsce.getMX()-jednaTrzecia,mojeMiejsce.getMY());
      Miejsce lwdl=new Miejsce(mojeMiejsce.getMX()-jednaTrzecia,mojeMiejsce.getMY()+jednaTrzecia);
      Miejsce pwgr=new Miejsce(mojeMiejsce.getMX()+jednaTrzecia,mojeMiejsce.getMY()-jednaTrzecia);
      Miejsce pwsr=new Miejsce(mojeMiejsce.getMX()+jednaTrzecia,mojeMiejsce.getMY());
      Miejsce pwdl=new Miejsce(mojeMiejsce.getMX()+jednaTrzecia,mojeMiejsce.getMY()+jednaTrzecia);
      
      //skasuj wszystkie poprzednich miejsc
      wszystkieMiejsca.clear();
      wszystkieMiejsca.add(new Miejsce[] {srodka});
      wszystkieMiejsca.add(new Miejsce[] {lwgr,pwdl});
      wszystkieMiejsca.add(new Miejsce[] {lwgr,srodka,pwdl});
      wszystkieMiejsca.add(new Miejsce[] {lwgr,lwdl,pwgr,pwdl});
      wszystkieMiejsca.add(new Miejsce[] {lwgr,lwdl,pwgr,pwdl,srodka});
      wszystkieMiejsca.add(new Miejsce[] {lwgr,lwsr,lwdl,pwgr,pwsr,pwdl});

      if (tablicaZKropkami.size()==0) {return;}
      Miejsce[] miejscaDoTyluKropek=wszystkieMiejsca.get(tablicaZKropkami.size()-1);
      
      //aktualizuj miejsca wszysktie kropek
      for(int i=0;i<tablicaZKropkami.size();i++)
      {
        Kropka jkropka=tablicaZKropkami.get(i);
        jkropka.setMiejsce(miejscaDoTyluKropek[i]);
      }
  }
  
  //Dodaj jedna kropka do Kostki
  //Nie dotykaj tej metody
  public void dodajKropke(Kropka krop)
  {
    if (tablicaZKropkami.size()>5)
    {
       println("Kostka: Nie ma miejsca do wiecej Kropek w tej kostce");
       return; 
    }
    if (!tablicaZKropkami.contains(krop))
    {
      krop.setSrednica(mojRozmiar.getRozmiar()/5);
      tablicaZKropkami.add(krop);
      aktualizujMiejscaKropek();
      
      if (tablicaZKropkami.size()==6)
      {
        for(Kropka jedKrop:tablicaZKropkami)
        {
          jedKrop.setFarba(new FarbaCzerwona());
        }
      }
      
    }
    else
    {
       println("Błąd: Ta kropka jest już dodana do Kostki"); 
    }
  }

  //Rysuje całą kostkę
  //Nie dotykaj tej metody
  public void rysuj()
  {
     mojaFarba.aplikuj();
     rectMode(CENTER);
     rect(mojeMiejsce.getMX(),mojeMiejsce.getMY(),
           mojRozmiar.getRozmiar(),mojRozmiar.getRozmiar(),5);
           
     for (Kropka jedKropka:tablicaZKropkami)
     {
       jedKropka.maluj();
     }
  }
  
  //Zmienia miejsce Kostki
  //Nie dotykaj tej metody
  public void setMiejsce(Miejsce mj)
  {
    mojeMiejsce=mj;
    aktualizujMiejscaKropek();
  }
  
}