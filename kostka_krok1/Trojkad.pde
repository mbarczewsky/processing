//Tworzy Trojkad
class Trojkad
{
  //Nie dotykaj tych zmienncyh
  private Farba mojaFarba; 
  private Miejsce mojeMiejsce;
  private int srednica=0;
  
  //Rysuje Trojkad
  //Nie dotykaj tej metody
  public void rysuj(Miejsce mj,int szer,Farba frb)
  {
     noStroke();
     if (frb!=null) {frb.aplikuj();}
     triangle(mj.getMX(), mj.getMY()-szer/2,
               mj.getMX()+szer/2, mj.getMY()+szer/2,
               mj.getMX()-szer/2, mj.getMY()+szer/2); // clockwise, starts at noon: x1, y1, x2, y2, x3, y3
  }
  
}