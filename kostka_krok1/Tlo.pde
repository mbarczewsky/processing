/**
* UWAGA:
* Nie dotknij ten kod - tu nic nie rób.
*
* Cały tło jest narysowany w tej klasie
*/
class Tlo
{
   //Rysuje tło
   public void narysuj()
   {
     background(#FF9F03);
     
     fill(0);
     stroke(0);
     textSize(40);
     textAlign(CENTER);
     text("KOSTKA DO GRY",400,50);
     line(200,55,600,55);
     
     rectMode(CENTER);
     rect(400,300,600,400,10);
   }
}