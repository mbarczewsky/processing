class Obraz
{
  private Czasomierz nastepnaKlatka = new Czasomierz(65);
  private Czasomierz przerwa = new Czasomierz(5000);
  private ArrayList<PImage> tablicaObrazu;
  private int posY;
  private int frame=0;
  private boolean animacjaTrwa=true;
  private boolean kolceDoGory=true;
  
  Obraz(String sciezka,int szer, int wys,int x, int y)
  {
    int tempCount=0;
    tablicaObrazu= new ArrayList<PImage>();
    PImage temp = loadImage(sciezka);
    posY=100;
    
    for(int i=0;i<y;i++){
      for(int j=0;j<x;j++){
        tablicaObrazu.add(tempCount,temp.get(j*szer,wys*i,szer,wys));
        tempCount++;
      }
    }
    
    tablicaObrazu.remove(41);
    tablicaObrazu.remove(40);
  }
  
  public void rysuj(int posX){
    if(przerwa.czasMinal())animacjaTrwa=true;
    
    if(animacjaTrwa){
      if(nastepnaKlatka.czasMinal()){
        if(kolceDoGory)frame++;
        else frame--;
      }
      if(!(frame<tablicaObrazu.size()-1)){
        kolceDoGory=false;
        animacjaTrwa=false;
      }
      if(frame<=0){
        kolceDoGory=true;
        animacjaTrwa=false;
      }
    }
    image(tablicaObrazu.get(frame),posX,posY);
  }
}

class GrupaKolcy{
  private Obraz[] tablicaKolcy= new Obraz[10];
  GrupaKolcy(){
    Obraz jedenKolec = new Obraz("kolec6x7-115x200.png",115,200,6,7);
    for(int i=0;i<tablicaKolcy.length;i++){
      tablicaKolcy[i]=jedenKolec;
    }
  }
  public void rysuj(){
    for(int i=0;i<tablicaKolcy.length;i++){
      tablicaKolcy[i].rysuj(i*115);
    }
  }
}

class Czasomierz{
  private int interval=0;
  private int ostatniCzas=0;
  
  Czasomierz(int interval){
    this.interval=interval;
  }
  
  public boolean czasMinal()
  {
    if ((millis()-ostatniCzas)>interval)
    {
      ostatniCzas=millis();
      return true;
    }
    else return false;
  }
}

GrupaKolcy pareKolcy;

void setup(){
  size(1150,300);
  frameRate(60);
  pareKolcy=new GrupaKolcy();
  
}
void draw(){
  background(#FFFFFF);
  pareKolcy.rysuj();
}
