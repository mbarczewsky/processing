Obraz facet;

class Obraz
{
  private ArrayList<PImage> tablicaObrazu;
  private int szer,wys;
  private int posX,posY;
  private int frame=0;
  private int ostatniCzas=0;
  private boolean animacjaTrwa=false;
  
  Obraz(String sciezka,int szer, int wys,int x, int y)
  {
    this.szer=szer;
    this.wys=wys;
    tablicaObrazu= new ArrayList<PImage>();
    PImage temp = loadImage(sciezka);
    posX=200;
    posY=200;
    int licznik=0;
    for(int i=0;i<y;i++){
      for(int j=0;j<x;j++){
        tablicaObrazu.add(i*y+j,temp.get(j*szer,wys*i,szer,wys));
        licznik++;
        if (licznik==14) return;
      }
    }
    //facet.tablicaObrazu.remove(15);
    //facet.tablicaObrazu.remove(14);
  }
  
  public void klikniecie(){
    if(mouseX<posX || mouseX>posX+szer)return;
    if(mouseY<posY || mouseY>posY+wys)return;
    animacjaTrwa=true;
  }
  
  public void rysuj(){
      if(animacjaTrwa){
        if(nastepnaKlatka()){
          posX+=5;
          frame++;
        }
        if(!(frame<tablicaObrazu.size()-1)){
          frame=0;
          animacjaTrwa=false;
          //return;
        }
    }
    image(tablicaObrazu.get(frame),posX,posY);
  }
  
  boolean nastepnaKlatka()
  {
    if ((millis()-ostatniCzas)>55)
    {
      ostatniCzas=millis();
      return true;
    }
    else return false;
  }
}

void setup(){
  size(800,600);
  frameRate(120);
  facet = new Obraz("jump4x4-200x201.png",200,201,4,4); //~~za duzo o 2 obrazki~~ naprawilem
  println(facet.tablicaObrazu.size());
}

void draw(){
  background(#FFFFFF);
  facet.rysuj();
}
void mouseClicked(){
  facet.klikniecie();
}
